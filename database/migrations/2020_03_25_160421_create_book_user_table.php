<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateBookUserTable extends Migration {
    public function up()
    {
		Schema::create('book_user', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->bigInteger('book_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
    
			$table->foreign('book_id')->references('id')->on('books')
						->onDelete('cascade')
						->onUpdate('cascade');
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
	}
	public function down()
	{
		Schema::table('book_user', function(Blueprint $table) {
			$table->dropForeign('book_user_book_id_foreign');
			$table->dropForeign('book_user_user_id_foreign');
		});
		Schema::drop('book_user');
	}
}