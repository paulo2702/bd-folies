<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('content');
            $table->BigInteger('ad_id')->unsigned();
            $table->foreign('ad_id')->references('id')->on('ads')
            ->onDelete('cascade');
            $table->BigInteger('acheteur_id')->unsigned();
            $table->foreign('acheteur_id')->references('id')->on('users')
            ->onDelete('cascade');
            $table->BigInteger('vendeur_id')->unsigned();
            $table->foreign('vendeur_id')->references('id')->on('users')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
