<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:airlock')->get('/user/me', function (Request $request) {
    if($request->user()->tokenCan('edit:post')){
        return $request->user();
    }
    else{
        return ['message'=>'you cant'];
    }

    
    //request()->user();
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*Route Api books*/
 Route::middleware('auth')->group(function () {
 Route::get('books', 'BibliothequeController@index')->name('books');
 Route::post('books', 'BibliothequeController@store')->name('books.store');
 Route::get('books/{id}', 'BibliothequeController@show')->name('books.show');
 Route::delete('books/{id}', 'BibliothequeController@destroy')->name('books.delete');
 });

