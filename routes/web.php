<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



/* Vue Routes*/
  
Auth::Routes(['verify'=>true]);
Route::get('/collection',function(){
  return view('collection');   
})->middleware('verified');

Route::get('/collection/bibliotheque', function () {
  return view('bibliotheque');
})->name('bibliotheque')->middleware('auth');

Route::get('/collection/annonces', function () {
  return view('annonces');
})->name('annonces')->middleware('auth');


/* Admin Routes*/

  Route::get('/admin', 'AdminController@admin')    
    ->middleware('is_admin')    
    ->name('admin');

  Route::get('users', 'AdminController@indexUser')->name('users.index')->middleware('is_admin');
  Route::get('users/{user}/edit','AdminController@editUser')->name('users.edit')->middleware('is_admin');
  Route::put('users/{user}','AdminController@updateUser')->name('users.update')->middleware('is_admin');
  Route::delete('users/{id}', 'AdminController@destroyUser')->name('users.destroy')->middleware('is_admin');
  Route::get('adminbooks', 'AdminController@indexBook')->name('adminbooks.index')->middleware('is_admin');
  Route::get('createbook','AdminController@createBook')->name('adminbooks.createbook')->middleware('is_admin');
  Route::post('storebook','AdminController@storeBook')->name('adminbooks.storebook')->middleware('is_admin');
  Route::delete('adminbooks/{id}', 'AdminController@destroyBook')->name('adminbooks.destroy')->middleware('is_admin');
  Route::get('ads', 'AdminController@indexAd')->name('adminads.index')->middleware('is_admin');
  Route::delete('ads/{id}', 'AdminController@destroyAd')->name('adminads.destroy')->middleware('is_admin');
  Route::get('image_uploads', 'AdminController@indexImage')->name('adminimages.index')->middleware('is_admin');
  Route::delete('image_uploads/{id}', 'AdminController@destroyImage')->name('adminimages.destroy')->middleware('is_admin');
  
  /*Contact Routes*/

  Route::get('contact','ContactController@create')->name('contact.create')->middleware('auth');
  Route::post('contact','ContactController@store')->name('contact.store')->middleware('auth');


  /*Ads Routes*/

  Route::get('annonces','AdController@index')->name('ads.index')->middleware('auth');
  Route::get('annonce','AdController@create')->name('ads.create')->middleware('auth');
  Route::post('annonce','AdController@store')->name('ads.store')->middleware('auth');
  Route::post('search','AdController@search')->name('ads.search')->middleware('auth');
 
  /*Message Routes*/

  Route::get('reponse','MessageController@index')->name('reponse.index')->middleware('auth');
  Route::get('message/{vendeur_id}/{ad_id}','MessageController@create')->name('message.create')->middleware('auth');
  Route::post('message','MessageController@store')->name('message.store')->middleware('auth');
  Route::post('reponse/{message}', 'MessageController@destroy')->name('message.destroy')->middleware('auth');

  /*Crudbook Routes*/
  
  Route::get('books', 'BookController@index')->name('books.index')->middleware('auth');
  Route::get('books/{book}/edit','BookController@edit')->name('books.edit')->middleware('auth');
  Route::put('books/{book}','BookController@update')->name('books.update')->middleware('auth');
  
  /*Dropzone Routes*/

  Route::post('/store', 'AdController@uploadImages')->middleware('auth');
  Route::post('/delete', 'AdController@deleteImage')->middleware('auth');

  /*Scan Route*/

  Route::get('/scan', function () {
    return view('scan');
  })->name('scan')->middleware('auth');
  
  
  /*Bibliotheque React Routes*/

  Route::get( 'bibliotheque/{path?}', function(){
    return view( 'bibliotheque' );
  })->where('path', '.*')->middleware('auth');
  
  
  
 

 
 
 
 
 
