<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageUpload extends Model
{
    protected $fillable = ['image_name','ad_id','user_id'];
    
    public function ad()
    {
        return  $this->belongsTo('App\Ad');
    }
    

    
}
