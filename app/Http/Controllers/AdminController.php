<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Book;
use App\Ad;
use App\ImageUpload;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;



class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function admin()
    {
        return view('layouts.adminpanel');
    }

    public function indexUser(){
        $users=User::all();
        return view('admin.adminuserindex',compact('users'));
    }

    public function editUser($id){
        $user = User::find($id);
        return view('admin.adminuseredit',compact('user'));
    }

    public function updateUser(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->save();
        return redirect()->route('users.index');
    }

    public function destroyUser($id)
    {   $user=User::find($id);
        $user->delete();
        return redirect()->route('users.index');
    }

    public function indexBook()
    {   
        $books=Book::with('users')
        ->join('book_user','book_user.book_id','=','books.id')->get();
        return view('admin.adminbookindex',compact('books'));
    }

    public function createBook(){
        return view('admin.createbook');
    }

    public function storeBook(Request $request ){

        $book = $request->validate([
            'title' => 'required',
            'cover_image_url' => 'required',
            'subtitle' => 'required',
            'author' => 'required',
            'book_description' => 'required',
            'publish_date' => 'required',
            'publisher' => 'required',
            'user_id'=>'required',
        ]);
        
        $book= new Book();
        $book->title=$request['title'];
        $book->cover_image_url=$request['cover_image_url'];
        $book->subtitle=$request['subtitle'];
        $book->author='["'.$request['author'].'"]';
        $book->book_description=$request['book_description'];
        $book->publish_date=$request['publish_date'];
        $book->publisher=$request['publisher'];
        $book->save();
        $user_id=$request['user_id'];
        $user=User::find($user_id);
        $user->books()->attach($book->id);
        return back()->with('success','La Bd a bien été ajouté');       
    }

    public function destroyBook($id)
    {   
        $book=Book::find($id);
        $book->delete();
        return redirect()->route('adminbooks.index');
    }

    public function indexAd(){
        $ads=Ad::all();
        return view('admin.adminadindex',compact('ads'));
    }

    public function destroyAd($id){
        
        $ad=Ad::find($id);
        $images = ImageUpload::where('ad_id','=',$id)->get();
        foreach($images as $image){
        $path = Storage::delete($image->image_name);
        $image->delete();
        }
        $ad->delete();
        return redirect()->route('adminads.index');
    } 
}
