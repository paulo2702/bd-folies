<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Ad;
use App\User;
use App\Message;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $messages = [];
        $ads = Ad::where('user_id','=',Auth::user()->id)->get();
        foreach ($ads as $ad) {
            foreach ($ad->messages as $message) {
               $messages[]=$message;
            }
        }
        return view('messages.reponse',compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $ad_id=$request['ad_id']; 
      return view('messages.message',compact('ad_id')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $message = $request->validate([
            'title' => 'required',
            'content' => 'required', 
        ]);

        $message=new Message();
        $message->title=$request['title'];
        $message->content=$request['content'];
        $message->user_id=Auth::user()->id;
        $message->ad_id=$request['ad_id'];
        $message->save();
        return redirect()->route('ads.index')->with('success','Votre message a bien été posté');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $message=Message::find($id);
        $message->delete();
        return redirect()->route('reponse.index');
    }
}   
