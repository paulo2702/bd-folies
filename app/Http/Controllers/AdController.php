<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AdStore;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Ad;
use App\User;
use App\ImageUpload;

class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
      $ads=DB::table('ads')->orderBy('created_at','DESC')->paginate(3); 
      $images=DB::table('image_uploads')
      ->join('ads','ads.id','=','ad_id')
      ->get();              
      return view('annonces.adindex',compact('ads','images'));   
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
      return view('annonces.adcreate');
      
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdStore $request)
    {  
       $user = Auth::user();
       $data = $request->all();
       $data['user_id'] = $user->id;
       $ad = Ad::create($data);
       return $ad;  
    }    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\$ads
     */
    public function search(Request $request){

        $words=$request->words;
        $ads =Ad::with('image_uploads')
        ->where('title','LIKE',"%$words%")
        ->orWhere('description','LIKE',"%$words%")
        ->orderBy('created_at','DESC')
        ->get();
        return $ads;    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */    
    public function uploadImages(Request $request) {
      $user = Auth::user();
      $images = $request->file('file');
      foreach($images as $image){
        $imageName= $image->store('images');
        $data  =ImageUpload::create(['image_name' => $imageName,'ad_id'=>$request->ad_id]);
      }
        
        return response()->json(["status" => "success", "data" => $data]);
    }
  
    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request  
     * @return \Illuminate\Http\Response
     */
    public function deleteImage(Request $request) {
        
      $filename =  $request->get('filename');
      ImageUpload::where('image_name', $filename)->delete();
      $path = Storage::delete($filename);
        return $filename;
    }

    
}
