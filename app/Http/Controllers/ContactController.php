<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactMail;
use App\Contact;

class ContactController extends Controller
{
    
     // Create Contact Form
     public function create() {
        return view('contact.contactcreate');
     }

     // Store Contact Form data
    public function store(Request $request) {

        // Form validation
        $data=request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'sujet'=>'required',
            'message' => 'required'
         ]);

        //  Store data in database
        Contact::create($data);

        Mail::to('paulohemery@yahoo.fr','admin')->send(new ContactMail($data));
        return back()->with('success', 'Nous avons recu votre message et nous vous remerçions de nous avoir contacter.');
     }


}
