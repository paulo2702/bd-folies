<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\User;
use Illuminate\Support\Facades\Auth;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        $id=Auth::id();
        $user= User::find($id) ;
        $books=$user->books; 
        return view('books.userbookindex',compact('books'));
     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::find($id);
        $book->author =trim($book->author,'[".."]');
        return view('books.userbookedit',compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $book = $request->validate([
            'title' => 'required',
            'subtitle' => 'required',
            'book_description' => 'required',
            'author' => 'required',
            'publish_date' => 'required',
            'publisher' => 'required', 
        ]);

        $book = Book::find($id);
        $book->title = $request->get('title');
        $book->subtitle = $request->get('subtitle');
        $book->book_description = $request->get('book_description');
        $book->author = '["'.$request->get('author').'"]';
        $book->publish_date = $request->get('publish_date');
        $book->publisher = $request->get('publisher');
        $book->save();
        return redirect()->route('bibliotheque')->with('success','Votre bd a bien été modifiée');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
