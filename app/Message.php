<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['title','content','ad_id','user_id'];
    protected $with = ['user'];
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function ad(){
        return $this->belongsto('App\Ad');
    }
}

