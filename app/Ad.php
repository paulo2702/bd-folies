<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $fillable = ['title', 'description', 'localisation','price','user_id'];
    protected $with=['user','messages','image_uploads'];
    
   public function user(){
       return $this->belongsTo('App\User');
   }

   public function image_uploads(){
        return  $this->hasMany('App\ImageUpload');
   }
   public function messages(){
       return $this->hasMany('App\Message');
   }
   
}
