                             
                    #WEB PROJECT: BDFOLIES#
    


    ##DESCRIPTION:

    Comic book archive website
    
    

    
    ##INSTALLATION:
    
    
    ###Clone the repository 
    
        git clone https://gitlab.com/paulo2702/bd-folies.git
    
    ###Switch to the repo folder
    
        cd bd-folies in Visual Studio Code
        
    ###Install all the dependencies using composer
    
        composer install
    
    ###Copy the example env file and make the required configuration changes in the .env file
    
        cp .env.example .env
        
    ###Run the database migrations (Set the database connection in .env before migrating)
    
        php artisan migrate
        
    ###Start the local development server
    
        php artisan serve
        
    
    
    ###Environment variables
    
        .env - Environment variables can be set in this file
        
    Note : You can quickly set the database information and other variables in this file and have the web site fully working.
    
    