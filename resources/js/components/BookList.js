import axios from 'axios';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';


class BookList extends Component {
  constructor() {
    super()
    this.state = {
      books: [],
      bookList:[],
      errors: [],
      activePage: 1,
    };
    this.handleDeleteBook = this.handleDeleteBook.bind(this);
    this.filterList = this.filterList.bind(this);
  };

  componentDidMount() {
    /* Request  data books*/
    axios.get('/api/books').then(response => {
      if (response.data.length) {
        response.data.forEach((book) => {
          let author = JSON.parse(book.author);
          book['author'] = author;
        })
        this.setState({
          books: response.data
        })
      } else {
        this.setState({
          books: 'Ajouter une BD pour commencer votre collection'
        })
      }
    })
  }
  
  /*Filter books by title*/
  
  filterList(e){
    const middle=  e.target.value.toLowerCase()
    let bookList = this.state.books;
      if(e.target.value!==''){
        bookList = bookList.filter(books => {
          return ((books.title.toLowerCase()).search(middle)>=0);
        });
        if(bookList[0]){
          this.setState({
            books: bookList,
          });
        };  
      }
      else{
        axios.get('/api/books').then(response => {
          if (response.data.length) {
            response.data.forEach((book) => {
              let author = JSON.parse(book.author);
              book['author'] = author;
            })
            this.setState({
              books: response.data
            })
          }
        })
      }   
  }
  /* Delete Book in collection*/
  handleDeleteBook(id, title) {
    const { books } = this.state;
    axios.post(`/api/books/${id}`, { _method: 'delete' })
      .then(response => {
        let newBooks = books.filter(e => e.id !== id);
        this.setState({
          books: newBooks,
        })
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        })
      })
  }
  render() {
    const { books} = this.state;
    if (!books.length) {
      return null;
    }
    let bookList = typeof books === 'object' ? books.map((book) => {
      
      return (
        <div className='list-book' key={book.id}>
          <Link className='list-group-item list-group-item-action d-flex justify-content-between'
            to={`/${book.id}`} >
              <div className="align">
                <div><img className='multi-cover-image' src={book.cover_image_url} alt={`${book.title} cover`}></img></div>
                  <p className='book-title'>{book.title}</p>
                  <p className='book-title'>{book.subtitle}</p>
                  <p className='book-author'>{book.author[0]}</p>
              </div>
          </Link>
          
          <button
            className='btn btn-primary btn-sm'
            onClick={() => this.handleDeleteBook(book.id, book.title)}><label>Supprimer la BD</label>
          </button>
        </div>
      )
    })
      : 'Ajouter une BD pour completer votre collection';

    return (
      <div className='booklist-container'>
        <div className='list'>
          <section className='list-header'>
            <h4 id="titrebooklist">VOS BANDES DESSINEES</h4>
            <section className="list-header-actions">
              <Link to='/create' className="btn btn-primary m-2">Ajouter une BD</Link>
              <a href='/books' className="btn btn-primary m-2">Modifier une BD</a> 
              <input type="text" className="search m-2" placeholder="Search BD" onChange={this.filterList}/>
            </section>
          </section>
          <div className='list-body'>
            {bookList}
          </div>
        </div>
      </div>
    )
  }
}

export default BookList;
