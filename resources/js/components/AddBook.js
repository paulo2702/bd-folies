import axios from 'axios';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class AddBook extends Component {
  
  constructor(props) {
    super()
    this.state = {
      search_term: '',
      book_results: [],
      errors: [],
    }
    this.handleFieldChange = this.handleFieldChange.bind(this)
    this.handleAddBook = this.handleAddBook.bind(this)
    this.bookData = this.bookData.bind(this);
    this.fetchBooks = this.fetchBooks.bind(this);
    this.onSubmitHandler = this.onSubmitHandler.bind(this);  
  }

  handleFieldChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }
  
  /* Add data book in database*/
  handleAddBook(bookToAdd) {
    const book = this.bookData(bookToAdd);
    const { history } = this.props;
    axios.post('/api/books', book)
      .then(response => {
        history.push('/')
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        })
      })
  }

  bookData(bookResults) {
    let title = bookResults.volumeInfo.title ? bookResults.volumeInfo.title : '';
    let subtitle = bookResults.volumeInfo.subtitle ? bookResults.volumeInfo.subtitle : '';
    let bookDescription = bookResults.volumeInfo.description ? bookResults.volumeInfo.description : '';
    let publishedDate = bookResults.volumeInfo.publishedDate ? bookResults.volumeInfo.publishedDate : '';
    let publisher = bookResults.volumeInfo.publisher ? bookResults.volumeInfo.publisher : '';
    let coverImageUrl = `https://books.google.com/books/content?id=${bookResults.id}&printsec=frontcover&img=1&zoom=1&source=gbs_api`
    let authors = [''];
    if (bookResults.volumeInfo.authors) {
      authors = bookResults.volumeInfo.authors;
    }
    let book = {
      title,
      subtitle,
      author: JSON.stringify(authors),
      book_description: bookDescription,
      publish_date: publishedDate,
      publisher,
      cover_image_url: coverImageUrl,
    }
    return book;
  }
  /*Request book data API Googlebooks*/
  async fetchBooks() {
    let API_URL = `https://www.googleapis.com/books/v1/volumes/`;
    // Ajax call to API using Axios
    const result = await axios.get(`${API_URL}?q=${this.state.search_term}
    &key=AIzaSyAobwmc04x8Yzc1PI6Fydlf-pRzuij3z2U&maxResults=32&country=fr`)
    .then((result)=>{
      
      this.setState({
          // Books result
          book_results: result.data,
      })
    }); 
  }

  // Submit handler
  onSubmitHandler(e) {
    e.preventDefault();
    this.fetchBooks();
  }

  render() {
    let { book_results } = this.state;
    let searchResults;

    if (book_results.items) {
      searchResults =
        <div className="results-container">
          {
            book_results.items.map((book, i) => {
              let authors = [''];
              if (book.volumeInfo.authors) {
                authors = book.volumeInfo.authors;
               
              }
              return (
                <div className="results-book" key={i}>
                  <div>
                    <img alt={`${book.volumeInfo.title} book`} src={`https://books.google.com/books/content?id=${book.id}&printsec=frontcover&img=1&zoom=1&source=gbs_api`} />
                    <div>
                      <h4 className="book-title">{book.volumeInfo.title}</h4>
                      <h4 className="book-title">{book.volumeInfo.subtitle}</h4>

                      {
                        authors.map((authors, i) => {
                          return (
                            <h6 key={i}>{authors}</h6>
                          )
                        })
                      }
                    </div>
                  </div>
                  <Link className='back-to-list-btn' to='/collection/bibliotheque'>
                    <button
                      className='btn btn-primary'
                      onClick={() => this.handleAddBook(book)}
                    >Ajouter BD</button>
                  </Link>  
                </div>
              );
            })
          }
        </div>
    }
    return (
      <div className='add-book-container'>
        <div className='add-book'>
          <section >
            <form  onSubmit={this.onSubmitHandler}>
              <label>
                <input
                  name='search_term'
                  placeholder="ISBN, Titre, ou Auteur"
                  type="search"
                  value={this.state.search_term}
                  onChange={this.handleFieldChange}
                />
                <button type="submit" className="btn btn-primary">Chercher</button>
                ou
                <a href='/scan' className="btn btn-primary mr-2  ">Scanner un Isbn</a>
              </label>
            </form>
          </section>
          {searchResults}
        </div>
        <Link className='navbar-brand' to='/collection/bibliotheque'><button className="back-to-list-btn">Retour Collection</button></Link>
       <h4>Si vous ne trouvez pas votre BD, pas de panique!! Contacter l'administrateur <a href="/contact">Contact</a></h4>
      </div>
    )
  }
}

export default AddBook;