import axios from 'axios';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Book extends Component {

  constructor(props) {
    super(props)
    this.state = {
      book: {}
    };
   
  }

  componentDidMount() {

    const bookId = this.props.match.params.id
    /* Request data for one book*/
    axios.get(`/api/books/${bookId}`).then(response => {
      let author = JSON.parse(response.data.author);
      
      response.data['author'] = author;

      this.setState({
        book: response.data,
      });

    })
  }

  render() {
    const { book } = this.state;
    if (!book.id) {
      return null;
    }
    let authors = <div>
      <p>Par: {book.author[0]}</p>
    </div>;
    if (book.author.length > 1) {
      let authors =
        <div>
          {
            book.author.map((author, i) => {
              return (
                <p key={i}>Par: {book.author[i]}</p>
              )
            })
          }
        </div>
    }

    
    
    return (
      
      <div className="book-container">
        <div className="book">
          <div className="book-header">
            <h2 className="book-title">{`${book.title} - ${book.subtitle}`}</h2>
          </div>
          <div className="book-body">
            <h3 className="auteur">{authors}</h3>
              <img className="single-cover-image" src={book.cover_image_url} alt={`${book.title} cover`}></img>
          </div>
            <p className="publish-date">Date de publication: {book.publish_date}</p>
                 <p className="publisher">Editeur: {book.publisher}</p>
            <div className="color"><p className="description">{book.book_description}</p></div>      
            <hr />
        </div>
        <Link className='navbar-brand' to='/collection/bibliotheque'><button className="back-to-list-btn">Retour Collection</button></Link>
      </div>
    )
  }
}

export default Book;