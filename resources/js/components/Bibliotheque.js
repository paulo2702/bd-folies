import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import BookList from './BookList';
import AddBook from './AddBook';
import Book from './Book';

/*React Router. Point d'entrée des composants*/
function Bibliotheque()  {
  
    return (
      <BrowserRouter forceUpdate={true}>
        <div>
          <Switch>
            <Route exact path='/collection/bibliotheque' component={BookList} />
            <Route path='/create' component={AddBook} />
            <Route path='/:id' component={Book} /> 
          </Switch>
        </div>
      </BrowserRouter>
      
    )
}
if(document.getElementById('bibliotheque')) {
  ReactDOM.render(<Bibliotheque />, document.getElementById('bibliotheque'));
}