<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>BD'S FOLIES</title>
    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/bibliotheque.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body style="background-color: #45348B;
        font-family: 'Comic Sans MS', sans-serif;">
    <header>
        <nav class="navbar">

            <div class="navbar-toggle" id="js-navbar-toggle">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </div>
            <div class="menu"><a id="a-titre">BD'S FOLIES</a></div>
            <ul class="main-nav" id="js-menu">
                <li><a class="nav-links" href="{{ url('/collection') }}">Collection</a></li>
            </ul>
        </nav>
    </header>
    @include('flash-message')
    <div id="bibliotheque"></div>

    <script src="{{mix('js/app.js')}}"></script>
    <script>
        let mainNav = document.getElementById('js-menu');
        let navBarToggle = document.getElementById('js-navbar-toggle');

        navBarToggle.addEventListener('click', function () {
            mainNav.classList.toggle('active');
        });

    </script>

</body>
</html>
