@extends('layouts.app1')

@section('content')

<div class="container">
    <h2>Deposer vos annonces</h2>
    <hr>

    <form action="{{route('ads.store')}}" method="POST" id="form" enctype="multipart/form-data">
        @csrf
        @method('POST')
        <div class="form-group">
            <label for="image"  style="color:white;">Ajouter vos images(3 images au maximum et 5 Mo pour chacune)</label>
            <div method="POST" action="{{url('/store')}}" class="dropzone" id="dropzone">
                <div class="dz-message">
                    <div class="col-xs-8">
                        <div class="message">
                            <p>Déposez vos photos ici ou cliquez</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="title" style="color:white;">Titre de l'annonce</label>
            <input type="text" class="form-control" id="title" aria-describedby="title" name="title" required>
        </div>

        <div class="form-group">
            <label for="description"  style="color:white;">Description de l'annonce</label>
            <textarea name="description" class="form-control " id="description" cols="30" rows="10" required></textarea>
        </div>
        <div class="form-group">
            <label for="localisation"  style="color:white;">Localisation</label>
            <input type="text" class="form-control " id="localisation" name="localisation" required>
        </div>
        <div class="form-group">
            <label for="price"  style="color:white;">Prix</label>
            <input type="text" class="form-control " id="price" name="price" required>
        </div>
        <button type="submit" class="btn btn-primary" id="submit-all">Soumettre votre annonce</button>
    </form>
</div>

@endsection
@section('extra-js')
<script type="text/javascript">
    Dropzone.prototype.defaultOptions.headers = {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    };
    Dropzone.options.dropzone = {
        maxFiles: 3,
        maxFilesize: 5,
        renameFile: function (file) {
            var dt = new Date();
            var time = dt.getTime();
            return time + file.name;
        },
        acceptedFiles: ".jpeg,.jpg,.png",
        addRemoveLinks: true,
        dictRemoveFile: 'Supprimer',
        timeout: 50000,
        uploadMultiple: true,
        parallelUploads: 3,
        autoProcessQueue: false,
        
        init: function () {
            var submitButton = document.querySelector("#submit-all")
            mydropzone = this;
            // Envoi des data du formulaire et récupération de la réponse
            // du serveur qui contient l'id du Ad créé.
            $('#form').on('submit', function (e) { 
                e.preventDefault();
                // empêche l'envoi du formulaire par le nav
                const form = e.target // < le formulaire
                const data = new FormData(form) // < les data du formulaire
                // Et on envoi
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/annonce',
                    type: 'POST',
                    data: data,
                    processData: false,
                    contentType: false,
                    done: onSuccess // < le callback pour récupérer la réponse
                }).done(onSuccess);
            })
            // ad_id va contenir l'id du Ad créé avec la 1er requête
            let ad_id;
            // Callback de la requête /store pour récupérer l'id du Ad
            function onSuccess(response) {
                // Récup de l'id du Ad créé
                ad_id = response.id;
                // Envoi des images
                mydropzone.processQueue();
            }
            // Pose d'un écouteur à l'envoi des images pour ajouter
            // l'identifiant du Ad avec l'envoi des images
            this.on("sending", function (file, xhr, formData) {
                formData.append("ad_id", ad_id);
            });
        },
        removedfile: function (file) {
            var name = file.upload.filename;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: '{{ url("/delete") }}',
                data: {
                    filename: name
                },
                success: function (data) {
                    console.log("L'image a bien été suprimée!!");
                },
                error: function (e) {
                    console.log(e);
                }
            });
            var fileRef;
            return (fileRef = file.previewElement) != null ?
                fileRef.parentNode.removeChild(file.previewElement) : void 0;
        },

        success: function (file, response) {
            console.log(response);
            window.location.href = "{{route('ads.index')}}";
        },
        error: function (file, response) {
            return false;
        }
    };

</script>
@endsection

