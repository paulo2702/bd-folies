@extends('layouts.app1')

@section('content')

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
        <form method="POST"action="{{route('ads.search')}}"onsubmit="search(event)"id="searchform">
        @csrf
            <div class="form-group">
                <input type="text" class="form-control" id="words">
                <button class="btn btn-primary mt-3">Rechercher</button>
            </div>
        </form>
        <div id="results">

    @foreach($ads as $ad)

    <div class="card mb-3 p-3" style="width: 100%;">
      <div class="card-body " >
        @foreach($images as $image)
          @if ($image->ad_id == $ad->id)
            <img  class="img-fluid img-thumbnail m-4" style="min-width:20%; height:150px;" src="{{asset($image->image_name )}}"   alt="">
          @endif 
        @endforeach
      </div>
      <div class="card-body">
        <h5 class="card-title">{{$ad->title}}</h5>
        <small>{{Carbon\Carbon::parse($ad->created_at)->diffForHumans()}}</small>
        <p class="card-text text-info">{{$ad->localisation}}</p>
        <p class="card-text">{{$ad->description}}</p>
        <p class="card-text">{{$ad->price}} euros</p>
        <a href="{{route('message.create',['vendeur_id'=>$ad->user_id,'ad_id'=>$ad->id])}}" class="btn btn-primary">Contacter le vendeur</a>
      </div>
    </div>
        @endforeach
    </div>
    {{$ads->links()}}
    </div>
  </div>
</div>

@endsection

@section('extra-js')
<script>
function search(event){
    event.preventDefault()
    
    const words=document.querySelector('#words').value;
    const url=document.querySelector('#searchform').getAttribute('action');
    axios.post(`${url}`, {
    words: words,
  })
  .then(function (response) {
   
    const ads= response.data
    console.log(ads)
    let results=document.querySelector('#results')
    results.innerHTML=''
    for(let i=0;i<ads.length;i++){
      let card=document.createElement('div')
      card.classList.add('card', 'mb-3')
      let cardImage=document.createElement('div')
      card.classList.add('card-body')
      ads[i].image_uploads.forEach(function(image){
      let img=document.createElement('IMG')
      img.classList.add('img-fluid', 'img-thumbnail', 'm-4')
      img.setAttribute('style','min-width:20%; height:150px;')
      img.src =image.image_name 
      cardImage.appendChild(img)
      });
      let cardBody=document.createElement('div')
      card.classList.add('card-body')
      let title=document.createElement('h5')
      title.classList.add('card-title')
      title.innerHTML=ads[i].title
      let createdat=document.createElement('small')
      createdat.innerHTML=ads[i].created_at
      let localisation=document.createElement('p')
      localisation.classList.add('card-text','text-info')
      localisation.innerHTML=ads[i].localisation
      let description=document.createElement('p')
      description.classList.add('card-text')
      description.innerHTML=ads[i].description
      let price=document.createElement('p')
      price.classList.add('card-text')
      price.innerHTML=ads[i].price
      let objLink = document.createElement('a')
      objLink.setAttribute('href','{{route('message.create',['vendeur_id'=>$ad->user_id ,'ad_id'=>$ad->id])}}')
      objLink.innerText ="Contacter le vendeur";
      objLink.classList.add('btn', 'btn-primary')
      cardBody.appendChild(title)
      cardBody.appendChild(createdat)
      cardBody.appendChild(localisation)
      cardBody.appendChild(description)
      cardBody.appendChild(price)
      cardBody.appendChild(objLink) 
      card.appendChild(cardImage)
      card.appendChild(cardBody)
      results.appendChild(card)
    }
  })
  .catch(function (error) {
    console.log(error);
  });
}
</script>
@endsection

