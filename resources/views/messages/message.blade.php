@extends('layouts.app1')

@section('content')
<div class="container">
    <h2 style="color:white;">Contacter le vendeur</h2>
    <hr>



    <form action="{{route('message.store')}}" method="POST">
        @csrf
        @method('POST')
        <div class="form-group">
            <label for="title" style="color:white;">Titre du message</label>
            <input type="text" class="form-control" id="title" aria-describedby="title" name="title" required>
        </div>
        <div class="form-group">
            <label for="content" style="color:white;">Message</label>
            <textarea name="content" class="form-control " id="content" cols="30" rows="10" required></textarea>
        </div>
        <input type="hidden" name="ad_id" value="{{$ad_id}}">
        <button type="submit" class="btn btn-primary">Envoyer</button>
    </form>
</div>

@endsection
