@extends('layouts.app1')

@section('content')
<div class="container">
    <h1>Messages</h1>

   
    <table class="table table-bordered table-dark table-responsive-stack  ">
    @foreach($messages as $message)
        <tbody>
            <tr>
                <td>{{$message->title}}</td>
                <td>{{$message->content}}</td>
                <td>{{getBuyerName($message->user_id)}}</td>
                <td>{{getBuyerMail($message->user_id)}}</td>
                <td>
                    <form action="{{ route('message.destroy',$message->id) }}" method="POST">
                        @csrf
                        @method('POST')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
        </tbody>
        @endforeach
    </table>

   

</div>

@endsection
