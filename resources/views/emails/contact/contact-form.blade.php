@component('mail::message')
# Bonjour,

Vous avez reçu un mail de la part de {{$data['name']}} ({{$data['email']}})

Sujet:
{{$data['sujet']}}

Message:
{{$data['message']}}




@endcomponent
