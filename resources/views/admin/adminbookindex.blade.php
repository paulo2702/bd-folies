@extends('layouts.adminpanel')

@section('admincrud')


<div class="container">

    <table class="table table-hover table-bordered table-dark table-responsive-md ">
        <thead>
            <tr>
                <th scope="col">User Id</th>
                <th scope="col">Book Id</th>
                <th scope="col">Titre</th>
                <th scope="col">Sous-Titre</th>
                <th colspan="2">Action</th>
            </tr>
        </thead>
        <tbody>
           
            @foreach($books as $book) 
            <tr>
                <td>{{$book->user_id}}</td>
                <td>{{$book->book_id}}</td>
                <td>{{$book->title}}</td>
                <td>{{$book->subtitle}}</td>
                <td>
                    <form action="{{ route('adminbooks.destroy',['id'=>$book->book_id]) }}" method="POST">
                        @csrf
                        @method('DELETE')

                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        
        </tbody>
    </table>

</div>
@endsection
