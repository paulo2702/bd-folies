@extends('layouts.adminpanel')
@section('admincrud')
<div class="container-fluid">
    <h2>Création de bd</h2>
    <form method="post" action="{{route('adminbooks.storebook')}}"enctype="multipart/form-data">
            @csrf
            @method('POST')
        <div class="form-group">
            <label for="user_id" style="color:white;">User Id :</label>
            <input type="text" class="form-control" name="user_id" required/>
        </div>

        <div class="form-group">
            <label for="cover_image_url" style="color:white;">Lien image :</label>
            <input type="text" class="form-control" name="cover_image_url" required />
        </div>

        <div class="form-group">
            <label for="title" style="color:white;">Titre :</label>
            <input type="text" class="form-control" name="title" required />
        </div>

        <div class="form-group">
            <label for="subtitle" style="color:white;">Sous-Titre :</label>
            <input type="text" class="form-control" name="subtitle" required />
        </div>

        <div class="form-group">
            <label for="book_description" style="color:white;">Description :</label>
            <textarea  class="form-control" name="book_description" required ></textarea>
        </div>

        <div class="form-group">
            <label for="author" style="color:white;">Auteur :</label>
            <input type="text" class="form-control" name="author" required />
        </div>

        <div class="form-group">
            <label for="publish_date" style="color:white;">Date de publication :</label>
            <input type="text" class="form-control" name="publish_date" required />
        </div>

        <div class="form-group">
            <label for="publisher" style="color:white;">Editeur :</label>
            <input type="text" class="form-control" name="publisher" required />
        </div>


        <button type="submit" class="btn btn-primary">Ajouter BD</button>
    </form>
</div>
@endsection