@extends('layouts.adminpanel')

@section('admincrud')


<div class="container">

    <table class="table table-hover table-bordered table-dark table-responsive-line ">
        <thead>
            <tr>
                <th scope="col">User Id</th>
                <th scope="col">Annonce Id</th>
                <th scope="col">Titre</th>
                <th scope="col">Description</th>
                <th scope="col">Prix</th>
                <th colspan="2">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($ads as $ad)
            <tr>
                <td>{{$ad->user_id}}</td>
                <td>{{$ad->id}}</td>
                <td>{{$ad->title}}</td>
                <td>{{$ad->description}}</td>
                <td>{{$ad->price}}</td>
                <td>
                    <form action="{{ route('adminads.destroy',$ad->id) }}" method="POST">

                        @csrf
                        @method('DELETE')

                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

</div>
@endsection
