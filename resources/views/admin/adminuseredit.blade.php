@extends('layouts.adminpanel')

@section('admincrud')

<div class="container-fluid">
    <h2>Modification Utilisateur</h2>
    <form method="post" action="{{ route('users.update', $user->id ) }}">
        <div class="form-group">
            @csrf
            @method('PUT')
            <label for="name">Nom :</label>
            <input type="text" class="form-control" name="name" value="{{ $user->name }}" />
        </div>

        <div class="form-group">
            <label for="email">Email :</label>
            <input type="text" class="form-control" name="email" value="{{ $user->email }}" />
        </div>
        <button type="submit" class="btn btn-primary">Modifier Utilisateur</button>
    </form>
</div>

@endsection
