@extends('layouts.adminpanel')
@section('admincrud')
<div class="container">

    <table class="table table-hover table-bordered table-dark table-responsive-line">
        <thead>
            <tr>

                <th scope="col">Nom</th>
                <th scope="col">Email</th>
                <th colspan="2">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>

                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td><a href="{{ route('users.edit', $user->id)}}" class="btn btn-primary" style="width:70.8px;">Edit</a></td>
                <td>
                    <form action="{{ route('users.destroy', $user->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

</div>

@endsection
