@extends('layouts.app')

@section('content')
<div class="container opac">
    @if(Session::has('success'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {{Session::get('success')}}
    </div>
    @endif
    <form action="{{ route('contact.store') }}" method="POST">
        @csrf

        <div class="form-group">
            <label style="color:white;">Nom</label>
            <input type="text" class="form-control {{ $errors->has('name') ? 'error' : '' }}" name="name" id="name"
                required>


            @if ($errors->has('name'))
            <div class="error">
                {{ $errors->first('name') }}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label style="color:white;">Email</label>
            <input type="email" class="form-control {{ $errors->has('email') ? 'error' : '' }}" name="email" id="email"
                required>

            @if ($errors->has('email'))
            <div class="error">
                {{ $errors->first('email') }}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label style="color:white;">Sujet</label>
            <input type="text" class="form-control {{ $errors->has('sujet') ? 'error' : '' }}" name="sujet" id="sujet"
                required>

            @if ($errors->has('sujet'))
            <div class="error">
                {{ $errors->first('sujet') }}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label style="color:white;">Message</label>
            <textarea class="form-control {{ $errors->has('message') ? 'error' : '' }}" name="message" id="message"
                rows="4" required></textarea>

            @if ($errors->has('message'))
            <div class="error">
                {{ $errors->first('message') }}
            </div>
            @endif
        </div>

        <button type="submit" class="btn btn-primary">Envoyer</button>
    </form>
</div>
@endsection
