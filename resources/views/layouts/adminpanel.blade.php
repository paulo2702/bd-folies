<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Admin</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>


    <!-- Fonts -->

    <link href="https://fonts.googleapis.com/css?family=Seymour+One&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="css/adminpanel.css">
<body>
    <header>
        <nav class="navbar">
            <div class="navbar-toggle" id="js-navbar-toggle">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </div>
            <div class="menu"><a id="a-titre">BD'S FOLIES</a></div>
            <ul class="main-nav" id="js-menu">
                <li><a class="nav-links" href="{{ url('/') }}">Home</a></li>
                <li><a class="nav-links" href="{{ url('/users') }}">Users</a></li>
                <li><a class="nav-links" href="{{url('/createbook')}}"> Create Books</a></li>
                <li><a class="nav-links" href="{{ url('/adminbooks') }}">Books</a></li>
                <li><a class="nav-links" href="{{ url('/ads') }}">Annonces</a></li>
            </ul>
        </nav>
    </header>
        @include('flash-message')
    <main class="py-4">
        @yield('admincrud') 
    </main>

    </div>
    <script>
        let mainNav = document.getElementById('js-menu');
        let navBarToggle = document.getElementById('js-navbar-toggle');

        navBarToggle.addEventListener('click', function () {
            mainNav.classList.toggle('active');
        });

    </script>
</body>
</html>
