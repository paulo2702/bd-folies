<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app2.name', 'BDS FOLIES') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Seymour+One&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app2.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<body style="background-color: #45348B;
        font-family: 'Comic Sans MS', sans-serif;">
    <header>
        <nav class="navbar">

            <div class="navbar-toggle" id="js-navbar-toggle">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </div>
            <div class="menu"><a id="a-titre">BD'S FOLIES</a></div>
            <ul class="main-nav" id="js-menu">
                <li><a class="nav-links" href="{{ url('/collection/bibliotheque') }}">Bibliotheque</a></li>
            </ul>
        </nav>
    </header>

    <main class="py-4">
        @yield('contentbook')
    </main>
    <script src="{{ asset('js/app.js') }}" type="text/js"></script>
    <script>
        let mainNav = document.getElementById('js-menu');
        let navBarToggle = document.getElementById('js-navbar-toggle');

        navBarToggle.addEventListener('click', function () {
            mainNav.classList.toggle('active');
        });

    </script>
</body>
<style>
   
    .table-responsive-line tr {
      display: flex;
      flex-direction: column;
    }

    .table-responsive-line  {
        font-weight: bold;
    }
    


    


</style>
</html>
