<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'BDS FOLIES') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Seymour+One&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/contact.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>

    <header>
        <nav class="navbar">
            <div class="navbar-toggle" id="js-navbar-toggle">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </div>
            <div class="menu"><a id="a-titre">BD'S FOLIES</a></div>
            <ul class="main-nav" id="js-menu">
                <li><a class="nav-links" href="{{ url('/') }}">Home</a></li>
            </ul>
        </nav>
    </header>
    <div class="main-img">

        <div id="imagebackground">

            <main class="py-4">
                @yield('content')
               

            </main>
        </div>
    </div>
    <script>
        let mainNav = document.getElementById('js-menu');
        let navBarToggle = document.getElementById('js-navbar-toggle');

        navBarToggle.addEventListener('click', function () {
            mainNav.classList.toggle('active');
        });

    </script>
</body>
<style>
.main-img {
  background: url('img/imageconnexion.jpg');
  background-position: center center;
  background-repeat: no-repeat;
  background-size: cover;
  height: 120vh;
  width: 100%;
}
.opac {
  opacity: 70%;
}
</style>

</html>
