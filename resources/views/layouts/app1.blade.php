<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app1.name', 'BDS FOLIES') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Seymour+One&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app1.css') }}" rel="stylesheet">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/min/dropzone.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/dropzone.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body style="background-color: #45348B;
        font-family: 'Comic Sans MS', sans-serif;">
    <header>
        <nav class="navbar">
            <div class="navbar-toggle" id="js-navbar-toggle">
                <i class="fa fa-bars" aria-hidden="true"></i>

            </div>
            <div class="menu"><a id="a-titre">BD'S FOLIES</a></div>

            <ul class="main-nav" id="js-menu">
                <li><a class="nav-links" href="{{ url('/collection') }}">Collection</a></li>
                <li><a class="nav-links" href="{{route('ads.create')}}">Ajouter une annonce</a></li>
                <li><a class="nav-links" href="{{route('ads.index')}}">Voir les annonces</a></li>
                <li><a class="nav-links" href="{{route('reponse.index')}}">Message</a></li>
            </ul>
        </nav>
    </header>
    @include('flash-message')
    <main class="py-4">
        @yield('content') 
    </main>

    <script>
        let mainNav = document.getElementById('js-menu');
        let navBarToggle = document.getElementById('js-navbar-toggle');

        navBarToggle.addEventListener('click', function () {
            mainNav.classList.toggle('active');
        });

    </script>
    @yield('extra-js')
</body>
</html>
