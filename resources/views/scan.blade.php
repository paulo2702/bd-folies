<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=640, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Scan</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/scan.css">
</head>

<body>
   
    <nav class="navbar">

        <div class="navbar-toggle" id="js-navbar-toggle">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </div>
        <div class="menu"><a id="a-titre">BD'S FOLIES</a></div>
        <ul class="main-nav" id="js-menu">
            <li><a class="nav-links" href="{{ url('/collection/bibliotheque') }}">Bibliotheque</a></li>
        </ul>
    </nav>

    <div id="camera"></div>

    <script src="https://serratus.github.io/quaggaJS/examples/js/quagga.min.js" type="text/javascript"></script>

    <script>
        Quagga.init({
            numOfWorkers: 4,
            frequency: 10,
            locate: true,
            inputStream: {
                name: "Live",
                type: "LiveStream",
                target: document.querySelector('#camera'),
                constraints: {
                    width: {
                        min: 280,
                        max:720
                    },
                    height: {
                        min: 480,
                        max: 480
                    },
                    aspectRatio: {
                        min: 1,
                        max: 100
                    },
                    facingMode: "environment", // or user
                    frameRate: 10,
                }
            },
            decoder: {
                readers: [
                    'ean_reader'
                ]
            },
            locator: {
                patchSize: "x-large", // x-small, small, medium, large, x-large
                halfSample: true
            }
        }, function (err) {
            if (err) {
                return console.log(err);
            }
            console.log("Initialization finished. Ready to start");
            Quagga.start();
        });

        Quagga.onProcessed(function (result) {
            var drawingCtx = Quagga.canvas.ctx.overlay,
                drawingCanvas = Quagga.canvas.dom.overlay;
            if (result) {
                if (result.boxes) {
                    drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")), parseInt(
                        drawingCanvas.getAttribute("height")));
                    result.boxes.filter(function (box) {
                        return box !== result.box;
                    }).forEach(function (box) {
                        Quagga.ImageDebug.drawPath(box, {
                            x: 0,
                            y: 1
                        }, drawingCtx, {
                            color: "green",
                            lineWidth: 2
                        });
                    });
                }
                if (result.box) {
                    Quagga.ImageDebug.drawPath(result.box, {
                        x: 0,
                        y: 1
                    }, drawingCtx, {
                        color: "#00F",
                        lineWidth: 2
                    });
                }
                if (result.codeResult && result.codeResult.code) {
                    Quagga.ImageDebug.drawPath(result.line, {
                        x: 'x',
                        y: 'y'
                    }, drawingCtx, {
                        color: 'red',
                        lineWidth: 3
                    });
                }
            }
        });

        Quagga.onDetected(function (result) {
            var isbn = result.codeResult.code;
            if (isbn.match(/^97[8|9]/)) {
                console.log(isbn);
                Quagga.stop();
            }
            /*Request isbn scan result in  google books Api*/
            $.ajax({
                dataType: 'json',
                url: 'https://www.googleapis.com/books/v1/volumes?q=isbn' + isbn +
                    '&key=AIzaSyAobwmc04x8Yzc1PI6Fydlf-pRzuij3z2U&maxResults=1&country=fr',
                success: handleResponse,
               
            });
            /*Store response book data in database*/
            function handleResponse(response) {
                $.each(response.items, function (i, item) {
                    var volumeInfo = item.volumeInfo
                    console.log(volumeInfo.industryIdentifiers);
                    var title = item.volumeInfo.title ? item.volumeInfo.title : '';
                    var subtitle = item.volumeInfo.subtitle ? item.volumeInfo.subtitle : '';
                    var bookDescription = item.volumeInfo.description ? item.volumeInfo.description :
                    '';
                    var publishedDate = item.volumeInfo.publishedDate ? item.volumeInfo.publishedDate :
                    '';
                    var publisher = item.volumeInfo.publisher ? item.volumeInfo.publisher : '';
                    var authors = [''];
                    if (item.volumeInfo.authors) {
                        authors = item.volumeInfo.authors;
                    }
                    var coverImageUrl =
                        `https://books.google.com/books/content?id=${item.id}&printsec=frontcover&img=1&zoom=1&source=gbs_api`
                    const book = {
                        title,
                        subtitle,
                        author: JSON.stringify(authors),
                        book_description: bookDescription,
                        publish_date: publishedDate,
                        publisher,
                        cover_image_url: coverImageUrl,
                    }
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        url: 'api/books',
                        type: 'POST',
                        data: book,
                        success: handleResponse,
                    });
                    window.location.href = "{{route('bibliotheque')}}";
                });
            }
        });
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</body>

</html>