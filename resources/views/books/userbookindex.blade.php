@extends('layouts.app2')

@section('contentbook')


<div class="container">
    <div>
        @foreach($books as $book)
        <table class="table table-bordered table-dark table-responsive-line">
           
            <tbody> 
            <tr>
                <td>{{$book->title}}</td>
                <td>{{$book->subtitle}}</td>
                <td>{{$book->book_description}}</td>
                <td>{{$book->author}}</td>
                <td>{{$book->publish_date}}</td>
                <td>{{$book->publisher}}</td>
                <td><a  href="{{ route('books.edit', $book->id)}}" class="btn btn-primary" style="width:120px;">Edit</a></td>
            </tr>
            </tbody>
        </table>
        @endforeach

    </div>


    @endsection
