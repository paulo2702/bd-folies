@extends('layouts.app2')

@section('contentbook')

<div class="container-fluid">
    <h2 style="color:white;">Formulaire de modification</h2>
    <form method="post" action="{{ route('books.update', $book->id ) }}">
        <div class="form-group">
            @csrf
            @method('PUT')
            <label for="title" style="color:white;">Titre :</label>
            <input type="text" class="form-control" name="title" value="{{ $book->title }}" />
        </div>

        <div class="form-group">
            <label for="subtitle"style="color:white;">Sous-Titre :</label>
            <input type="text" class="form-control" name="subtitle" value="{{ $book->subtitle }}" />
        </div>

        <div class="form-group">
            <label for="book_description"style="color:white;">Description :</label>
            <textarea  class="form-control" name="book_description">{{ $book->book_description }}</textarea>
        </div>

        <div class="form-group">
            <label for="author"style="color:white;">Auteur :</label>
            <input type="text" class="form-control" name="author" value="{{ $book->author }}"/>
        </div>

        <div class="form-group">
            <label for="publish_date"style="color:white;">Date de publication :</label>
            <input type="text" class="form-control" name="publish_date" value="{{ $book->publish_date }}" />
        </div>

        <div class="form-group">
            <label for="publisher" style="color:white;">Editeur :</label>
            <input type="text" class="form-control" name="publisher" value="{{ $book->publisher }}" />
        </div>
        
        <button type="submit" class="btn btn-primary">Modifier BD</button>
    </form>
</div>

@endsection
