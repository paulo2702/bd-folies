<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>BDS FOLIES</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Seymour+One&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    @auth
    <script src="{{mix('js/app.js')}}" defer></script>
    @endauth

    <!-- Styles -->
    <link rel="stylesheet" href="css/welcome.css">
    
</head>

<body>
    <header>
        <nav class="navbar">
            @if (Route::has('login'))
            <div class="navbar-toggle " id="js-navbar-toggle">

                <i class="fa fa-bars" aria-hidden="true"></i>
            </div>

            <div class="menu"><a id="a-titre">BD'S FOLIES</a></div>
            <ul class="main-nav" id="js-menu">
                @auth



                <li><a class="nav-links" href="{{ url('/contact') }}">Contact</a></li>
                <li><a class="nav-links" href="{{ url('/collection') }}">Collection</a></li>

                <li><a class="nav-links" href="{{ route('logout') }}" onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a></li>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                @else



                <li><a class="nav-links" href="{{ route('login') }}">Login</a></li>

                @if (Route::has('register'))
                <li><a class="nav-links" href="{{ route('register') }}">Register</a></li>
                @endif
                @endauth
                @endif
            </ul>
        </nav>
    </header>

    <img class="img-responsive" id="imagehome" src="img/imagehome.png" alt="" width="100%" height="auto">
    <footer class="page-footer" id="footer">
        <div class="footer text-center py-3" id="colorfont"> Paulo 2020</div>
    </footer>
    
    <script>
        let mainNav = document.getElementById('js-menu');
        let navBarToggle = document.getElementById('js-navbar-toggle');

        navBarToggle.addEventListener('click', function () {
            mainNav.classList.toggle('active');
        });

    </script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>

</body>

</html>
