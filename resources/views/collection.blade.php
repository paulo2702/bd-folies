<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>BDS FOLIES</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Seymour+One&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    @auth
    <script src="{{mix('js/app.js')}}" defer></script>
    @endauth
    <!-- Styles -->
    <link rel="stylesheet" href="css/collection.css">
</head>

<body>
    <header>
        <nav class="navbar">
            <div class="navbar-toggle" id="js-navbar-toggle">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </div>
            <div class="menu"><a id="a-titre">BD'S FOLIES</a></div>
            <ul class="main-nav" id="js-menu">
                @if(auth()->user()->isAdmin())
                <li><a class="nav-links" href="{{ url('/admin') }}">Admin</a></li>
                @endif
                <li><a class="nav-links" href="{{ url('/') }}">Home</a></li>
                <li><a class="nav-links" href="{{ url('/collection/bibliotheque') }}">Bibliotheque</a></li>
                <li><a class="nav-links" href="{{ url('/annonces') }}">Annonces</a></li>


            </ul>
        </nav>
    </header>
    <img class="img-responsive" id="imagecollection" src="img/collection.jpg" alt="" width="100%">
    <footer class="page-footer" id="footer">
        <div class="footer-copyright text-center py-3" id="colorfont"> Paulo 2020</div>
    </footer>

    <script>
        let mainNav = document.getElementById('js-menu');
        let navBarToggle = document.getElementById('js-navbar-toggle');

        navBarToggle.addEventListener('click', function () {
            mainNav.classList.toggle('active');
        });

    </script>


</body>

</html>
